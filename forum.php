<?php

require 'vendor/autoload.php';

use judahnator\BlockChainForum\Commands\ForumClientCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new ForumClientCommand());
$application->setDefaultCommand('forum:run');
$application->run();
