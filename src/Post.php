<?php

namespace judahnator\BlockChainForum;

use judahnator\BlockChain\Block;
use Symfony\Component\Console\Exception\LogicException;


/**
 * Class Post
 *
 * @property-read string $title
 * @property-read string $contents
 * @property-read \DateTime $created_at
 *
 * @package judahnator\BlockChainForum
 */
class Post implements Jsonable
{

    const ATTRIBUTES = [
        'title',
        'contents',
        'created_at'
    ];

    private $attributes;

    public $block;

    public function __construct(string $title, string $contents, \DateTime $created_at, Block $ownerBlock = null)
    {
        $this->attributes = [
            'title' => $title,
            'contents' => $contents,
            'created_at' => $created_at
        ];
        $this->block = $ownerBlock;
    }

    public function __get($name)
    {
        if (in_array($name, self::ATTRIBUTES)) {
            return $this->attributes[$name];
        }
    }

    public function excerpt(int $length = 20): string
    {
        return substr($this->title ?: $this->contents, 0, $length);
    }

    public static function fromBlock(Block $block): self
    {
        return new self(
            $block->data->title,
            $block->data->contents,
            (new \DateTime())->setTimestamp($block->data->created_at),
            $block
        );
    }

        /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize(): array
    {
        return [
            'title' => $this->title,
            'contents' => $this->contents,
            'created_at' => $this->created_at->getTimestamp()
        ];
    }

    /**
     * Take a given JSON string and create a Jsonable object from it.
     * @return Jsonable
     */
    public static function jsonUnSerialize(string $json): Jsonable
    {
        $data = json_decode($json);
        return new self(
            $data->title,
            $data->contents,
            $data->created_at
        );
    }

    /**
     * Attempts to return this blocks parent.
     *
     * @return Post
     * @throws LogicException
     */
    public function parent() {
        if ($this->block && $this->block->previous) {
            return self::fromBlock($this->block->previous);
        }
        throw new LogicException('This post has no parent');
    }

    public function reply(string $title, string $contents): self
    {
        $reply = new self($title, $contents, new \DateTime());
        $replyBlock = $this->block->createChild((object)$reply->jsonSerialize());
        $reply->block = $replyBlock;
        return $reply;
    }
}