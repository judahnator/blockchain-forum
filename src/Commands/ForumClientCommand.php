<?php

namespace judahnator\BlockChainForum\Commands;


use judahnator\BlockChain\BlockChain;
use judahnator\BlockChain\Drivers\FileDriver;
use judahnator\BlockChainForum\Post;
use PhpSchool\CliMenu\CliMenu;
use PhpSchool\CliMenu\CliMenuBuilder;
use RedAnt\Console\Helper\SelectHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class ForumClientCommand extends Command
{

    protected function configure()
    {
        $this
            ->setName('forum:run')
            ->setDescription('Starts the forum application');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->getHelperSet()
            ->set(new SelectHelper(), 'select');

        BlockChain::load(new FileDriver(__DIR__.'/../../data/Blocks'));

        $menu = (new CliMenuBuilder())
            ->setTitle('Posts Index');

        foreach (BlockChain::originBlock()->children() as $block) {
            $post = Post::fromBlock($block);
            $menu->addItem($post->excerpt(), function(CliMenu $menu) use ($post, $output, $input) {
                $this->displayPost($post, $output, $input);
                $menu->close();
            });
        }

        $menu->build()->open();
    }

    /**
     * @param Post $post
     * @throws \PhpSchool\CliMenu\Exception\InvalidTerminalException
     */
    private function displayPost(Post $post, OutputInterface $output = null, InputInterface $input = null) {
        (new CliMenuBuilder())

            ->setTitle("{$post->excerpt()} - {$post->created_at->format('m/d/Y H:i:s')}")

            ->addLineBreak()

            ->addStaticItem($post->contents)

            ->addLineBreak()

            ->addItem("Reply", function(CliMenu $menu) use ($post, $output, $input) {
                $menu->close();
                $response = $this
                    ->getHelper('question')
                    ->ask(
                        $input,
                        $output,
                        new Question('What do you have to say about this?'.PHP_EOL)
                    );
                $newPost = $post->reply('', $response);
                $this->displayPost($newPost);
            })

            ->addSubMenu("Comments")
            ->setTitle("Replies to this post")
            ->addItems(
                array_map(
                    function(array $blockData) use ($output, $input) {
                        $subPost = Post::fromBlock($blockData['block']);
                        return [
                            $subPost->excerpt(),
                            function(CliMenu $menu) use ($subPost, $output, $input) {
                                $menu->close();
                                $this->displayPost($subPost, $output, $input);
                            }
                        ];
                    },
                    $post->block->children()
                )
            )
            ->addLineBreak()
            ->end()

            ->addLineBreak()

            ->addItem('Previous Post', function (CliMenu $menu) use ($post, $output, $input) {
                try {
                    $this->displayPost($post->parent(), $output, $input);
                    $menu->close();
                }catch (\LogicException $exception) {
                    $output->writeln('This post has no parent');
                }
            })

            ->build()
            ->open();
    }

}